# Cloudstream Repository Instructions

* Cloudstream's Discord Server: https://discord.gg/RgbktAuXEu
* My Discord Server: https://destronia.com/discord or https://discord.com/invite/rvpbyWy

---
How to use:
1. Go to Settings > Extensions > Add Repository
2. Copy relevant url into the Repository URL section
3. Give it a name
4. Download
5. Go into the repo, and install any extension you want from it

## Links:

* **English**: https://codeberg.org/Stormunblessed/storm-ext/raw/branch/main/eng.json
* **Multi**: https://codeberg.org/cloudstream/cloudstream-extensions-multilingual/raw/branch/builds/repo.json
* **Hexated**: https://codeberg.org/Stormunblessed/storm-ext/raw/branch/main/hexa.json
* **Storm**: https://codeberg.org/Stormunblessed/storm-ext/raw/branch/main/storm.json
* **Kronch**: https://codeberg.org/Stormunblessed/storm-ext/raw/branch/main/kronch.json
* **Arab**: https://codeberg.org/Mark_Eshia/arab/raw/branch/builds/repo.json
* **LikDev**: https://codeberg.org/JakeWasHere7/likdev256-tamil-providers/raw/branch/builds/repo.json
* **Storm Backup**: https://pastebin.com/raw/7YRxz84P
* **Kronch Backup**: https://pastebin.com/raw/4d6S0gVU
* **Hexated Backu**p: https://pastebin.com/raw/mjDBFX3R
* **English Backup**: https://pastebin.com/raw/tX6F1aKt

----

## Repo Shortcodes (may not work) (Red Cloudstream Logo App only):

- Kronch - Kronch
- Multi - Multi
- English - EngRepo
- Hexated Repo - n7IQBDM
- Arabic Repo - r7IQ1yt
- Horis - l7IQ7DJ
- DSH - M7IWqme
- Stormunblessed providers repository - l7IWex2
- Eddy's Repo - w7IWtCZ
- Darkdemon Repo - N7IWTb3
- LikDev - Z7IWUXT
- CakesTwix Repo - f7IWSq9
- NSFW Repo - nsfwrepo

## Alternative Links (ARCHIVED, May not work):
- Kronch https://cutt.ly/Kronch
- Multi https://cutt.ly/Multi
- English https://cutt.ly/EngRepo
- Hexated Repo https://cutt.ly/n7IQBDM
- Arabic Repo https://cutt.ly/r7IQ1yt
- Horis https://cutt.ly/l7IQ7DJ
- DSH https://cutt.ly/M7IWqme
- Stormunblessed providers repository https://cutt.ly/l7IWex2
- Eddy's Repo https://cutt.ly/w7IWtCZ
- Darkdemon Repo https://cutt.ly/N7IWTb3
- LikDev https://cutt.ly/Z7IWUXT
- CakesTwix Repo https://cutt.ly/f7IWSq9
- NSFW Repo https://cutt.ly/nsfwrepo